//
//  ECSSFlipsideViewController.h
//  Elizabethtown College Student Senate
//
//  Created by Matt Shank on 6/17/13.
//  Copyright (c) 2013 Student Senate. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ECSSFlipsideViewController;

@protocol ECSSFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(ECSSFlipsideViewController *)controller;
@end

@interface ECSSFlipsideViewController : UIViewController

@property (weak, nonatomic) id <ECSSFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
