//
//  ECSSMainViewController.h
//  Elizabethtown College Student Senate
//
//  Created by Matt Shank on 6/17/13.
//  Copyright (c) 2013 Student Senate. All rights reserved.
//

#import "ECSSFlipsideViewController.h"

#import <CoreData/CoreData.h>

@interface ECSSMainViewController : UIViewController <UIWebViewDelegate, ECSSFlipsideViewControllerDelegate> {
    IBOutlet UILabel *titleLabel;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UIButton *news;
@property (weak, nonatomic) IBOutlet UIButton *calendar;
@property (weak, nonatomic) IBOutlet UIButton *mail;
@property (weak, nonatomic) IBOutlet UIButton *food;
@property (weak, nonatomic) IBOutlet UIButton *athletics;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *back;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *forward;

//- (void) initTopToolbarAbove:(UIWebView *)webView;
//- (void) initBottomToolbarBelow:(UIWebView *)webView;

//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
//- (void)webViewDidStartLoad:(UIWebView *)webView;

@end
