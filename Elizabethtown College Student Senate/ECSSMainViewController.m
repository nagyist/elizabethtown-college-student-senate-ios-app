//
//  ECSSMainViewController.m
//  Elizabethtown College Student Senate
//
//  Created by Matt Shank on 6/17/13.
//  Copyright (c) 2013 Student Senate. All rights reserved.
//

#import "ECSSMainViewController.h"

@interface ECSSMainViewController ()

@property (nonatomic) UIToolbar *topToolbar;
@property (nonatomic) UIToolbar *bottomToolbar;

@end

@implementation ECSSMainViewController

@synthesize news = _news;
@synthesize calendar = _calendar;
@synthesize mail = _mail;
@synthesize food = _food;
@synthesize athletics = _athletics;

@synthesize back = _back;
@synthesize forward = _forward;
@synthesize topToolbar = _topToolbar;
@synthesize bottomToolbar = _bottomToolbar;

UIWebView *webView;
UILabel *topToolbarLabel;


- (IBAction)newsClicked {
    [self openWebsite:@"http://www.etownian.com" withTitle:@"The Etownian"];
}

- (IBAction)athleticsClicked {
    [self openWebsite:@"http://www.etownbluejays.com/mobile" withTitle:@"Etown Blue Jays"];
}


- (UIToolbar *)topToolbar
{
    if(_topToolbar == nil) {
        _topToolbar = [[UIToolbar alloc] init];
        _topToolbar.barStyle = UIBarStyleDefault;
        _topToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        [_topToolbar sizeToFit];
        _topToolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    
        NSMutableArray *items = [[NSMutableArray alloc] init];
    
        [items addObject:[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeWebView)]];
    
        [_topToolbar setItems: items animated:NO];
    
        // Add the toolbar to the main view
        [self.view addSubview:_topToolbar];
    }
    
    return _topToolbar;
}

- (UIToolbar *)bottomToolbar
{
    if(_bottomToolbar == nil) {
        _bottomToolbar = [[UIToolbar alloc] init];
        _bottomToolbar.barStyle = UIBarStyleDefault;
        _bottomToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [_bottomToolbar sizeToFit];
        _bottomToolbar.frame = CGRectMake(0, self.view.frame.size.height-30, self.view.frame.size.width, 30);
        
        NSMutableArray *items = [[NSMutableArray alloc] init]; // Array to hold bottom toolbar items
        
        [items addObject:[self back]];
        
        // Generate and add fixed space
        UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedSpace.width = 40;
        [items addObject:fixedSpace];
        
        [items addObject:[self forward]];
        
        // Set the items on the bottom toolbar
        [_bottomToolbar setItems: items animated:NO];
        
        // Initially disable the back and forward buttons
        [items[0] setEnabled:NO];
        [items[2] setEnabled:NO];
    }

    return _bottomToolbar;
}

- (UIBarButtonItem *)back {
    if (_back == nil) {
        if(self.view.frame.size.width == 640) {
            _back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@2x.png"] landscapeImagePhone:[UIImage imageNamed:@"back@2x.png"] style:UIBarButtonItemStylePlain target:webView action:@selector(goBack)];
        }
        else
            _back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] landscapeImagePhone:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:webView action:@selector(goBack)];
    }
    return _back;
}

- (UIBarButtonItem *)forward {
    if (_forward == nil) {
        if(self.view.frame.size.width == 640) {
            _forward = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"forward@2x.png"] landscapeImagePhone:[UIImage imageNamed:@"forward@2x.png"] style:UIBarButtonItemStylePlain target:webView action:@selector(goForward)];
        }
        else
            _forward = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"forward.png"] landscapeImagePhone:[UIImage imageNamed:@"forward.png"] style:UIBarButtonItemStylePlain target:webView action:@selector(goForward)];
    }
    return _forward;
}

- (void) openWebsite:(NSString *)URL withTitle:(NSString *)toolbarTitle {
    webView = [[UIWebView alloc] init];
    
    // Add the top toolbar to the main view
    [self.view addSubview:self.topToolbar];
    
    // Add the bottom toolbar to the main view
    [self.view addSubview:self.bottomToolbar];
    
    [webView setFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height-40-30)];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
    webView.delegate = self;
    [[self view] addSubview:webView];
    
    // Add the toolbar title
    topToolbarLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    topToolbarLabel.text = toolbarTitle;
    topToolbarLabel.backgroundColor = [UIColor clearColor];
    topToolbarLabel.textColor = [UIColor whiteColor];
    topToolbarLabel.font = [UIFont boldSystemFontOfSize:18];
    [topToolbarLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:topToolbarLabel];
}

- (void) closeWebView //:(UIWebView *)webView AndTopToolbar:(UIToolbar *)topToolbar AndBottomToolbar:(UIToolbar *)bottomToolbar AndToolbarLabel:(UILabel *)toolbarLabel {
{
    [webView removeFromSuperview];
    [_topToolbar removeFromSuperview];
    [_bottomToolbar removeFromSuperview];
    [_back setEnabled:NO];
    [_forward setEnabled:NO];
    [topToolbarLabel removeFromSuperview];
}

- (void)viewDidLoad
{
    [titleLabel setFont:[UIFont fontWithName:@"BirchStd" size:55]];
     
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(ECSSFlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (webView.canGoBack)
        [_back setEnabled:YES];
    else
        [_back setEnabled:NO];
    if (webView.canGoForward)
        [_forward setEnabled:YES];
    else
        [_forward setEnabled:NO];
}

@end
