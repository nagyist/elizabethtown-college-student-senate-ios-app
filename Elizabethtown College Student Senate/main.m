//
//  main.m
//  Elizabethtown College Student Senate
//
//  Created by Matt Shank on 6/17/13.
//  Copyright (c) 2013 Student Senate. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ECSSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECSSAppDelegate class]));
    }
}
